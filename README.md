# Linked open data

## Problems
* What cluster of datasets can I use for my research (or otherwise) project?
* What datasets, of my options, are machine readable and valid?
* How can I avoid low impact correlations/linkages?

## Solutions
* Compute the overlap between datasets when assessing them for suitability.
* Trawl the data portals in question, computing basic, automated scores for a few measurables (validity, machine readability, data integrity)
* Produce a multi-modal network model, which we query against association via tag, organization, metadata (column names, basically), geographical and time domains.

The interface to such a network model would consist of a web UI, presenting a text/tag input, checkboxes for boolean filters, various filter. Results would be individual datasets, leading to a detail page with a prominent 'these datasets can be linked' area (like Amazon related products), and checkboxes to include fundamental datasets (e.g. statistical areas, budget data, weather, etc).

## Implementation
1. Get all the metadata for a ckan, socrata instance or other (i.e. Open Council Data Platform API) portal
2. Quick annotate machine readability based on format (pdf, docx -> no. json, yaml -> yes).
4. Check if the resource has been downloaded (by gs://<bucket>/<resource-id>), exclude if found.
3. Download in parallel all potentially machine readable datasets (this will take _a while_).
4. Store in bucket gs://<bucket>/resource-id>.
5. Run validity by data type (initial support: geographical validity, strictly tabular data).
6. Run machine readability checks.

# Installation
You'll need python v3.4 or later (for the core enum class. attrs needs v3 for introspection).
A simple way to get this (and the project dependencies) is via the Anaconda distribution, available in the full-sized variant (Anaconda)[https://www.continuum.io/downloads], or (Miniconda)[https://conda.io/miniconda.html].

Once that's installed, run:
```bash
conda env create -f environment.yml
```

Note: system-wide geos and gdal libraries won't work properly, as conda seems to fiddle with ld path.

To enter the environment:
```bash
source activate super-portal
```

Note that the main script isn't intended to be run locally, as it assumes a fairly resource and bandwidth rich environment (namely a cloud hosted VM), and for certain sections relies on pyspark.

# Data

Data is available publically at:

* data.gov.au - https://storage.googleapis.com/super-portal/data.gov.au.tar.gz
* data.vic.gov.au - https://storage.googleapis.com/super-portal/data.vic.gov.au.tar.gz


# Synopsis

The frontend was completed in Polymer, and is effectively feature complete - the user specifies the seed dataset by keyword (in the search box), and suggestions appear in the results column, along with all the ABS tables on the right.

Ingestion into postgresql works as expected, unfortunately due to time and manpower constraints the API is only partially completed - package searches work, but not linking.
For the purposes of demonstrating the intended functionality, the 'download bundle' button points to a static zip of a few likely ABS datasets and one or two data.gov.au, data.vic.gov.au.