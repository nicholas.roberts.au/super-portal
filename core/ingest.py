import asyncio
from models.models import *
import jsonlines
from sqlalchemy_aio import ASYNCIO_STRATEGY
from sqlalchemy import (
    Column, Integer, MetaData, Table, Text, create_engine, select)
from sqlalchemy.schema import CreateTable, DropTable
ROOT = "/home/nicholas/ckan-linked-data/"

async def validate_resources(resource_list):
    def attach_dummy_vals(res):
        res['machine_readability'] = 5
        res['detected_formats'] = ["SHP", "GEOJSON"]
        return res
    return map(attach_dummy_vals, resource_list)
async def go():
    engine = create_engine(
        # In-memory sqlite database cannot be accessed from different
        # threads, use file.
        'postgresql:///super-portal', strategy=ASYNCIO_STRATEGY
    )

    # Create the tables
    for tbl in [packages, organizations, resources]:
        await engine.execute(DropTable(tbl))
        await engine.execute(CreateTable(tbl))

    conn = await engine.connect()

    # populate the tables
    with jsonlines.open(ROOT+"output/resource_list.jsonl", "r") as reader:
        resource_list = reader.read()
    with jsonlines.open(ROOT+"output/package_list.jsonl", "r") as reader:
        package_list = reader.read()
    with jsonlines.open(ROOT+"output/organization_list.jsonl", "r") as reader:
        organization_list = reader.read()
    resource_list = await validate_resources(resource_list)
    res_package_pairs = []
    for pack in package_list:
        res_ids = map(lambda r: (r['id'], pack['metadata']['id']), pack['metadata']['resources'])
        res_package_pairs += list(res_ids)


    res_package_dict = dict(res_package_pairs)
    inserts = []
    for res in resource_list:
        _id = res['metadata']['id']
        name = res['metadata']['name']
        _format = res['metadata']['format']
        detected_formats = res['detected_formats']
        mr = res['machine_readability']
        package = res_package_dict[_id]

        inserts.append(conn.execute(resources.insert().values(
            id=_id, name=name, format=_format,
            detected_formats=detected_formats,
            package_id=package,
            machine_readable=mr)))

    await asyncio.gather(*inserts)
    inserts = []
    for org in organization_list:
        _id = org['metadata']['id']
        name = org['metadata']['name']
        inserts.append(conn.execute(organizations.insert().values(
            id=_id,name=name
        )))

    await asyncio.gather(*inserts)
    inserts = []
    for pack in package_list:
        _id = pack['metadata']['id']
        name = pack['metadata']['name']
        title = pack['metadata'].get('title', '')
        tag_ids = pack['metadata'].get('tags', [])
        resource_ids =list(map(lambda r: r['id'], pack['metadata']['resources']))
        org = pack['metadata'].get('organization', {}).get('id', '')
        inserts.append(conn.execute(packages.insert().values(
            id=_id,name=name,resources=resource_ids,organization=org,
            title=title, metadata=
        )))

    await asyncio.gather(*inserts)

    await conn.close()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(go())