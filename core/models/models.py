import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

metadata = sa.MetaData()
packages = sa.Table(
    'packages', metadata,
    sa.Column('id', sa.Text, primary_key=True),
    sa.Column('name', sa.Text),
    sa.Column('title', sa.Text),
    sa.Column('organization', sa.Text),
    sa.Column('resources', postgresql.ARRAY(sa.Text)),
)
organizations = sa.Table(
    'organizations', metadata,
    sa.Column('id', sa.Text, primary_key=True),
    sa.Column('name', sa.Text),
)

resources = sa.Table(
    'resources', metadata,
    sa.Column('id', sa.Text, primary_key=True),
    sa.Column('name', sa.Text),
    sa.Column('format', sa.Text),
    sa.Column('detected_formats', postgresql.ARRAY(sa.Text)),
    sa.Column('machine_readable', sa.Integer),
    sa.Column("fields", postgresql.ARRAY(sa.Text)),
    sa.Column("package_id", sa.Text)
)