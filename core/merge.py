import geopandas as gpd
import jsonlines
import sys
import magic
import glob
import os
import pprint
    
UNZIPPED_DIR = os.path.join("..", "data", "Unzipped")
RESOURCES_DIR = os.path.join("..", "data", "resources")

# Takes two file paths (e.g to downloaded data packs)
# If either is a zipped shapefile or KMZ, extracts the data
# If the first file is a point geographic data file and the 
# second file is a polygon geographic data file, does a spatial
# merge on the two files, then saves the result to the file
# specified by the third argument
def merge_files(path1, path2, out):
    file1 = process_file(path1)
    file2 = process_file(path2)
    if file1 is not None and file2 is not None:
        #try:
            left = gpd.read_file(file1)
            right = gpd.read_file(file2)
            result = spatial_merge(left, right)
            result.to_file(out, driver="GeoJSON")
            return True
        #except:
            #pass
    return False

def spatial_merge(left, right):
    # Get rid of any missing rows
    left = left.dropna()
    right = right.dropna()
    print("left", left.length)
    print("right", right.length)
    # Make sure both data sources are using the same coordinate reference system
    right.to_crs(left.crs)
    return gpd.sjoin(left, right)

# Takes an identifier, returns the path to a spatial data file or None
def process_file(path):
    #GEOJSON_DIR = os.path.join("..", "data", "GeoJSON")
    identifier = path.split("/")[-1]
    try:
        type = magic.from_file(path)
    except FileNotFoundError:
        return None
    if type.startswith("Zip archive data"):
        outdir = os.path.join(UNZIPPED_DIR, identifier)
        os.system("unzip -n -d {0} {1}".format(outdir, path))
        #if filename.endswith(".kmz"):
        files = glob.glob(os.path.join(outdir, "*")) + \
                glob.glob(os.path.join(outdir, "*", "*"))
        for file in files:
            if file.endswith(".shp"):
                print(file)
                return file
            if file.endswith(".kml"):
                new_file = "{0}.geojson".format(file[:-4])
                os.system("ogr2ogr -f \"GeoJSON\" {1} {0}.geojson".format(file, new_file))
                return new_file
    elif type.startswith("ESRI Shapefile"):
        return path

    #elif filename.endswith(".geojson"):
        #return filename
    return None

def process_all_files():
    with jsonlines.open("../data/data.vic.gov.au/resource_list.jsonl", "r") as reader:
        resources = reader.read()
    fs = [process_file(os.path.join(RESOURCES_DIR, resource["identifier"]))
                for resource in resources]
    fs = [f for f in fs if f is not None]
    pprint.pprint(fs)
    #for resource in resources:
        #format = resource["metadata"]["format"]
        #if "kmz" in format or "kml" in format or "shp" in format or "shape" in format:
            #print(format, resource["identifier"])


def main():
    if len(sys.argv) < 4:
        print("Usage: python merge.py outfile point_data_file shape_data_file column1 column2 ...")
        print("If columns are not specified, returns all columns")
    outfile = sys.argv[1]
    points = gpd.read_file(sys.argv[2])
    shapes = gpd.read_file(sys.argv[3])
    merged = spatial_merge(points, shapes)
    if len(sys.argv) >= 5:
        items = sys.argv[4:]
        # GeoJSON files have to have a geometry column
        if not "geometry" in items:
            items.append("geometry")
        merged = merged.filter(items=items)
    merged.to_file(outfile, driver="GeoJSON")

if __name__ == "__main__":
    main()
