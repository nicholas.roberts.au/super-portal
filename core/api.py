import asyncio
from models.models import *
from sanic import Sanic
from sanic.response import json
from sanic_cors import CORS, cross_origin
import inspect
import sys
import zipfile
import aiofiles

import time
api = Sanic()
cors = CORS(api, resources={r"/api/*": {"origins": "*"}})

from sqlalchemy_aio import ASYNCIO_STRATEGY
from sqlalchemy import (
    Column, Integer, MetaData, Table, Text, create_engine, select)
from sqlalchemy.schema import CreateTable, DropTable

engine = create_engine(
    # In-memory sqlite database cannot be accessed from different
    # threads, use file.
    'postgresql:///super-portal', strategy=ASYNCIO_STRATEGY
)

@api.route("/api/get/bundle", methods=["POST"])
async def generateBundle(request):
    datasets = request.json['sets']        
    return json({"msg": "not implemented"})

@api.route("/api/get/search_packages", methods=["POST"])
async def searchPackages(request):
    search_string = request.json['search']
    async with engine.connect() as conn:
        package_res = await conn.execute(packages.select(packages.c.title.contains(search_string)).limit(10))
        package_list = await package_res.fetchall()
        return json(package_list)

@api.route("/api/get/dataset_links", methods=["POST"])
async def getLinks(request):
    _id = request.json['id']
    print(request.json)
    async with engine.connect() as conn:
        package = await (await conn.execute(packages.select(packages.c.id == _id))).first()
        pack_resources = package.resources
        tags = package.tags
        org = package.organization
        # other datasets in organization + 1 point
        # has address in headers + 1 point - this is its own query
        # overlapping bounding boxes - this is its own query
        # is big - filter in each query
        # has any of the headers of input dataset - this is its own query
        # has the same number of fields - filter
        # has categorical fields, and with same set of unique categories in any field (that match ours)
        # query 1 - package to package organization matches
        same_orgs = await (await conn.execute(packages.select(packages.c.organization == org))).fetchall()
        # query 2 - exact header match between resources
        same_tags = []
        # for res in pack_resources:
        #     print(res)
        #     fields = (await (await conn.execute(resources.select(resources.c.id == res))).first()).fields
        #     all_fields += fields
        # overlapping_fields = await conn.execute(resources.select(resources.c.overlaps(all_fields)))
        return json({"orgs": same_orgs})
if __name__ == "__main__":
    if len(sys.argv) == 1:
        port == 80
    else:
        port = int(sys.argv[1])
    api.run(host="0.0.0.0", port=port)