from enum import Enum, EnumMeta

class MRMeta(EnumMeta):
    def __getitem__(self, name):
        try:
            # transform to upper case
            super().__getitem__(name.upper())
        except KeyError as e:
            print("Warning: format not recognized", e)
            return -1

# Note - direct format names are minimum values.
class MachineReadableEnum(Enum, metaclass=MRMeta):
    # 0 nope, not machine readable
    ZIP = 0

    # 1 maybe, with OCR - only useful for NLP
    PDF = 1
    # 2 structured text, need to know what you're looking at
    XML = 2
    HTML = 2
    # 3 no headers, but straightforward to process
    CSV = 3
    # 4 yes, machine readable
    KML = 4
    KMZ = 4
    SHP = 4
    GEOJSON = 4



# stub for machine readability categorizer
class MRCategorizor(object):
    def quick_categorize(self, obj):
        return MachineReadableEnum[obj['format']]

    def thorough_categorize(self, obj):
        for format in MachineReadableEnum:
            if format.name in obj['format']:
                return format
        return MachineReadableEnum[obj['format']]
