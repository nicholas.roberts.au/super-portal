FROM continuumio/anaconda3

COPY core $HOME/core
COPY environment.yml $HOME/environment.yml
RUN conda env update -f environment.yml -n root
EXPOSE 80
CMD ["python", "$HOME/core/api.py"]